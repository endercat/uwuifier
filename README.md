# uwuifier
this is a silly little program that makes text extra kawaii!

- ⚡ blazingly fast
- 🌸 kawaii~
- 😺 meowing
- 😳 very sus

## examples
> Hello, I am Endercat, the greatest cat to ever exist.

hewwo, i am endewcat, the gweatest cat to evew exist. (*≧ω≦)

> Cats are incredibly fascinating creatures. They exhibit a wide range of behaviors, from their graceful leaps to their playful pounces. Their soft fur and mesmerizing eyes make them utterly endearing. Whether they're curled up for a cozy nap or chasing after a toy, there's something undeniably charming about these fuzzy feline friends.

cats awe incwedibwy fascinating cweatuwes. (｡♥‿♥｡) they exhibit a wwide wange of behaviows, fwom theiw gwacefuw weaps to theiw pwayfuw pounces. ^_^' theiw soft fuw and mesmewizing eyes make them uttewwy endeawing. ^-^' wwhethew they'we cuwwed up fow a cozy nap ow chasing aftew a toy, thewe's something undeniabwy chawming about these fuzzy fewine fwiends. ^o^

## installation

### linux
1. clone the repo

```
git clone https://github.com/endercat126/uwuifier
cd uwuifier
```

2. ensure dependencies are installed
- make
- python (tested on 3.11.4)

3. install
```
make install
```

### windows & macos
- figure it out yourself


## changelog
(emoji support coming soon)

0.7.1
- no longer requires argcomplete
- uses the pypi version of ender-ansi
- added exit handling (ctrl+c, ctrl+d)

0.7
- complete rewrite
- added a config file
- made the output look pretty
- some more useful options
- now uses argparse

0.6
- initial release


## warning
dont use this around maxwell gameing its too cute for him

## license
copyright (c) 2023 endercat126. this software is licensed under the permissive mit license.
